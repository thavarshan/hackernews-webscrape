import pprint


def sort_stories_by_votes(stories):
    """Sort hacker news articles by most voted."""
    return sorted(stories, key=lambda k: k['votes'], reverse=True)


def create_hackernews(links, subtext):
    """Extract hacker news articles from web scraped content."""
    hackernews = []

    for index, item in enumerate(links):
        title = links[index].getText()
        href = links[index].get('href', None)
        vote = subtext[index].select('.score')

        if len(vote):
            points = int(vote[0].getText().replace(' points', ''))

            if points > 99:
                hackernews.append({
                    'title': title,
                    'link': href,
                    'votes': points
                })

    return sort_stories_by_votes(hackernews)
