from flask import Flask, render_template
from scraper import create_hackernews
import requests
from bs4 import BeautifulSoup


app = Flask(__name__)
response = requests.get('https://news.ycombinator.com/news')
soup = BeautifulSoup(response.text, 'html.parser')
links = soup.select('.storylink')
subtext = soup.select('.subtext')


@app.route('/')
def index():
    return render_template(
        'index.html',
        articles=create_hackernews(links, subtext)
    )
